import React, { useContext } from 'react'
import Player from 'react-player'
import { RoomContext } from './app'
import { FillBox } from './layout'
import { useTheme } from '@chakra-ui/core'
const Video = () => {
  const { stream } = useContext(RoomContext)
  const theme = useTheme()
  return (
    <FillBox>
      <Player url={stream} height="100%" width="100%" playing={true} muted={true} volume={1} />
    </FillBox>
  )
}

export default Video
