import React, { useContext } from 'react'
import { Flex, useTheme, Text } from '@chakra-ui/core'
import { RoomContext } from './app'
import { CenteredBox } from './layout'
import Player from 'react-player'

const VideoStrip = () => {
  const theme = useTheme()
  const { peers } = useContext(RoomContext)
  return (
    <Flex
      backgroundColor={theme.colors.blackAlpha[700]}
      color={theme.colors.gray[100]}
      flexDirection="row"
      gridAutoFlow="row wrap"
      position="absolute"
      bottom="1rem"
      left="1rem"
      right="1rem"
      align="center"
      wrap="wrap"
      padding="0.7rem"
      paddingX="1.5rem"
      fontSize="2rem"
      borderRadius={10}
      zIndex={1000}
      overflowX="auto"
      height="180px"
    >
      {Object.keys(peers).length === 0 && (
        <CenteredBox>
          <Text fontSize="3xl">No Connected Peers</Text>
        </CenteredBox>
      )}
      {Object.keys(peers).length > 0 &&
        Object.keys(peers)
          .filter(_peer => peers[_peer]._remoteStreams && peers[_peer]._remoteStreams.length > 0)
          .map(_id => {
            return (
              <Flex key={_id} flexDirection="column" width="200px" borderRadius={6} marginRight={3}>
                <Player
                  url={peers[_id]._remoteStreams[0]}
                  height="100%"
                  width="100%"
                  playing={true}
                  muted={false}
                  volume={1}
                  style={{ flex: 1 }}
                />
                <Flex
                  padding={1}
                  backgroundColor={theme.colors.blackAlpha[700]}
                  justifyContent="center"
                  position="absolute"
                  bottom="0"
                  width="200px"
                >
                  <Text fontSize="1rem">{_id}</Text>
                </Flex>
              </Flex>
            )
          })}
    </Flex>
  )
}

export default VideoStrip
