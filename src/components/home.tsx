import React, { useState } from 'react'
import { CenteredBox } from './layout'
import { Flex, useTheme, Input, Button } from '@chakra-ui/core'
import slugify from 'slugify'
import { FiCommand } from 'react-icons/fi'
import { useHistory } from 'react-router-dom'
import hri from 'human-readable-ids'
const Home = () => {
  const theme = useTheme()
  const [roomName, setRoomName] = useState<string>(hri.hri.random())
  const history = useHistory()
  return (
    <CenteredBox props={{ backgroundColor: theme.colors.gray[700], color: theme.colors.gray[400] }}>
      <Flex
        flexDirection="row"
        justify="space-between"
        backgroundColor={theme.colors.blackAlpha[700]}
        padding={8}
        borderRadius={6}
      >
        <Input
          placeholder="Room Name"
          width="400px"
          value={roomName}
          borderRadius={2}
          color={theme.colors.gray[800]}
          onChange={(e: any) => {
            const value = e.target.value.replace(/ /g, '-')
            setRoomName(slugify(value, { lower: true }))
          }}
        />
        <Button
          variantColor="green"
          marginLeft={2}
          leftIcon={FiCommand}
          onClick={() => {
            history.push(`/${roomName}`)
          }}
        >
          Start or Join Room
        </Button>
      </Flex>
    </CenteredBox>
  )
}

export default Home
