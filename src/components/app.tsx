import React, { useEffect, useState } from 'react'
import { hot } from 'react-hot-loader/root'
import { useToast, useTheme, useToastOptions } from '@chakra-ui/core'
import { useRouteMatch } from 'react-router-dom'
import superheroes from 'superheroes'
import swarm from 'webrtc-swarm'
import signalhub from 'signalhub'
import slugify from 'slugify'
import { FillBox } from './layout'
import Loading from './loading'
import Header from './header'
import Video from './video'
import VideoStrip from './video-strip'
interface IRoomContext {
  roomName: string
  user: string
  stream: MediaStream
  peers: {
    [id: string]: any
  }
  notify: (
    title: string,
    status?: 'info' | 'error' | 'success' | 'warning',
    props?: useToastOptions
  ) => void
  leave: () => Promise<void>
}
export let RoomContext: React.Context<IRoomContext>
const App = () => {
  const [user] = useState<string>(slugify(superheroes.random(), { lower: true }))
  const [stream, setStream] = useState<MediaStream>()
  const [sw, setSw] = useState<any>()
  const [peers, setPeers] = useState<{
    [id: string]: any
  }>({})
  const toast = useToast()
  const theme = useTheme()
  const match: { params: { id: string } } = useRouteMatch()
  const roomName = match.params.id
  const notify = (
    title: string,
    status: 'info' | 'error' | 'success' | 'warning' = 'info',
    props?: useToastOptions
  ) => {
    toast({ ...props, title, status, position: 'bottom-right' })
  }
  const context: IRoomContext = {
    roomName,
    user,
    stream,
    notify,
    peers,
    leave: async () => {
      return sw.close()
    },
  }
  RoomContext = React.createContext(context)
  useEffect(() => {
    navigator.mediaDevices
      .getUserMedia({
        video: true,
        audio: {
          echoCancellation: true,
        },
      })
      .then(_stream => {
        const hub = signalhub(roomName, ['https://signalhub-jccqtwhdwc.now.sh'])
        const _sw = swarm(hub, {
          uuid: user,
          stream: _stream,
        })
        _sw.on('peer', (peer: any, id: string) => {
          peers[id] = peer
          setPeers({ ...peers })
        })
        _sw.on('disconnect', (peer: any, id: string) => {
          delete peers[id]
          setPeers({ ...peers })
        })
        setStream(_stream)
        setSw(_sw)
      })
  }, [])
  return (
    <RoomContext.Provider value={context}>
      <FillBox props={{ backgroundColor: theme.colors.gray[700], color: theme.colors.gray[400] }}>
        {!stream && <Loading />}
        {stream && (
          <FillBox>
            <Header />
            <Video />
            <VideoStrip />
          </FillBox>
        )}
      </FillBox>
    </RoomContext.Provider>
  )
}

export default hot(App)
