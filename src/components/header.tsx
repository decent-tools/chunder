import React, { useContext, useEffect } from 'react'
import { Flex, useTheme, Text, Tag, useClipboard } from '@chakra-ui/core'
import {
  FiTarget,
  FiUser,
  FiMic,
  FiVideo,
  FiUsers,
  FiCopy,
  FiLogOut,
  FiVideoOff,
  FiMicOff,
} from 'react-icons/fi'
import { useLocalStorage } from 'react-use'
import { RoomContext } from './app'
import { ToolbarButton } from './layout'
import { useHistory } from 'react-router-dom'
const Header = () => {
  const theme = useTheme()
  const history = useHistory()
  const [showVideo, setShowVideo] = useLocalStorage('showVideo', true)
  const [unmute, setUnmute] = useLocalStorage('unmute', true)
  const { onCopy } = useClipboard(window.location.href)
  const { roomName, user, peers, leave, stream } = useContext(RoomContext)
  useEffect(() => {
    if (!showVideo) {
      stream.getVideoTracks().forEach(_track => {
        _track.stop()
        stream.removeTrack(_track)
      })
    } else {
      navigator.mediaDevices
        .getUserMedia({
          video: true,
          audio: unmute ? { echoCancellation: true } : false,
        })
        .then(_stream => {
          stream.addTrack(_stream.getVideoTracks()[0])
        })
    }
  }, [showVideo])
  useEffect(() => {
    if (!unmute) {
      stream.getAudioTracks().forEach(_track => {
        _track.stop()
        stream.removeTrack(_track)
      })
    } else {
      navigator.mediaDevices.getUserMedia({
        video: showVideo,
        audio: { echoCancellation: true },
      })
    }
  }, [unmute])
  return (
    <Flex
      as="nav"
      backgroundColor={theme.colors.blackAlpha[700]}
      color={theme.colors.gray[100]}
      flexDirection="row"
      position="absolute"
      top="1rem"
      left="1rem"
      right="1rem"
      align="center"
      justify="space-between"
      wrap="wrap"
      padding="0.7rem"
      paddingX="1.5rem"
      fontSize="2rem"
      borderRadius={10}
      zIndex={1000}
    >
      <Flex flexDirection="row" justify="flex-start" alignItems="center" flex={1}>
        <FiTarget />
        <Text marginLeft={2} marginRight={2} lineHeight="1.3rem" whiteSpace="nowrap">
          {roomName}
        </Text>
        <ToolbarButton Icon={FiCopy} props={{ marginRight: 2 }} onClick={onCopy} />
        <Tag variantColor="red" rounded="full" fontSize="1.3rem" marginLeft={2}>
          <FiUsers />
          <Text marginLeft={2} whiteSpace="nowrap">
            {Object.keys(peers).length + 1} users
          </Text>
        </Tag>
      </Flex>
      <Flex flexDirection="row" flex={1} alignItems="center" justifyContent="center">
        <ToolbarButton
          Icon={showVideo ? FiVideo : FiVideoOff}
          props={{ marginRight: 2 }}
          onClick={() => {
            setShowVideo(!showVideo)
          }}
        />
        <ToolbarButton
          Icon={unmute ? FiMic : FiMicOff}
          onClick={() => {
            setUnmute(!unmute)
          }}
          props={{
            marginRight: 2,
          }}
        />
      </Flex>
      <Flex
        flexDirection="row"
        justify="space-between"
        alignItems="center"
        justifyContent="flex-end"
        flex={1}
      >
        <FiUser />
        <Text marginLeft={2} marginRight={2}>
          {user}
        </Text>
        <ToolbarButton
          Icon={FiLogOut}
          onClick={async () => {
            await leave()
            stream.getTracks().forEach(_track => {
              _track.stop()
              stream.removeTrack(_track)
            })
            history.push('/')
          }}
          props={{
            marginRight: 2,
          }}
        />
      </Flex>
    </Flex>
  )
}

export default Header
