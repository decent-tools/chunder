import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/app'
import { Switch, Route, HashRouter } from 'react-router-dom'
import { ThemeProvider, CSSReset } from '@chakra-ui/core'
import Home from './components/home'
ReactDOM.render(
  <ThemeProvider>
    <CSSReset />
    <HashRouter>
      <Switch>
        <Route path="/:id">
          <App />
        </Route>
        <Route exact={true} path="/">
          <Home />
        </Route>
      </Switch>
    </HashRouter>
  </ThemeProvider>,
  document.getElementById('root')
)
